import React, { Component } from "react";
import { connect } from "react-redux";

class PhoneCart extends Component {
  renderTongGiaTien = () => {
    let tongTien = 0;
    this.props.gioHang.map(
      (phone) => (tongTien += phone.giaBan * phone.soLuong)
    );
    return tongTien;
  };
  renderPhoneCartWithMap = () => {
    return this.props.gioHang.map((phone, index) => {
      let { tenSP, maSP, giaBan, hinhAnh, soLuong } = phone;
      let img = hinhAnh.src_img;
      let i = hinhAnh.index_img;
      return (
        <tr key={index}>
          <td>{maSP}</td>
          <td>{tenSP}</td>
          <td>{giaBan}</td>
          <td>
            <img width={50} src={img[i]} alt="dienthoai" />
          </td>
          <td>
            <button
              onClick={() => {
                soLuong > 1 && this.props.thayDoiSoLuong(maSP, -1);
              }}
              className="btn btn-outline-warning mr-2">
              -
            </button>
            {soLuong}
            <button
              onClick={() => {
                this.props.thayDoiSoLuong(maSP, 1);
              }}
              className="btn btn-outline-warning ml-2">
              +
            </button>
          </td>
          <td>{giaBan * soLuong}</td>
          <td>
            <button
              onClick={() => {
                this.props.xoaGioHang(maSP);
              }}
              className="btn btn-danger">
              Delete
            </button>
          </td>
        </tr>
      );
    });
  };
  render() {
    if (this.props.gioHang.length >= 1) {
      return (
        <div className="container p-5">
          <h2 className="text-center">Phone Cart</h2>
          <table className="table table-striped table-inverse mt-5">
            <thead className="thead-inverse">
              <tr>
                <th>Mã sp</th>
                <th>Tên sp</th>
                <th>Giá Bán</th>
                <th>Hình Ảnh</th>
                <th>Số Lượng</th>
                <th>Tổng tiền</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>{this.renderPhoneCartWithMap()}</tbody>
          </table>
          <p className="text-center">
            <strong>
              Tổng số tiền cần phải thanh toán là : {this.renderTongGiaTien()}
            </strong>
          </p>
        </div>
      );
    }
  }
}

const mapStateToProps = (state) => {
  return { gioHang: state.gioHangReducer.gioHang };
};

const mapDispatchToProps = (dispatch) => {
  return {
    xoaGioHang: (maSP) => {
      const action = {
        type: "XOA_GIO_HANG",
        maSP: maSP,
      };
      dispatch(action);
    },
    thayDoiSoLuong: (maSP, soLuong) => {
      const action = {
        type: "THAY_DOI_SO_LUONG",
        maSP: maSP,
        soLuong: soLuong,
      };
      dispatch(action);
    },
  };
};
export default connect(mapStateToProps, mapDispatchToProps, null)(PhoneCart);
