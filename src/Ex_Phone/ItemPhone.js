import React, { Component } from "react";
import { connect } from "react-redux";

class ItemPhone extends Component {
  RenderPhonewithMap = () => {
    return this.props.data_phone.map((phone, index) => {
      let { tenSP, giaBan, hinhAnh, maSP } = phone;
      let img = hinhAnh.src_img;
      let i = hinhAnh.index_img;
      return (
        <div key={index} className="col-4">
          <div className="card border-primary h-100 text-center">
            <img className="card-img-top" src={img[i]} alt="dienthoai" />
            <div className="card-body">
              <h4 className="card-title">{tenSP}</h4>
              <p className="card-text">{giaBan}</p>
              <div className="mb-3 text-left">
                <span
                  onClick={() => {
                    this.props.thayDoiDienThoai(maSP, 0);
                  }}
                  className="black_color mr-3"></span>
                <span
                  onClick={() => {
                    this.props.thayDoiDienThoai(maSP, 1);
                  }}
                  className="white_color"></span>
              </div>
              <div className="d-flex justify-content-between">
                <button
                  onClick={() => {
                    this.props.xemChiTiet(phone);
                  }}
                  className="btn btn-outline-info">
                  Xem chi tiết
                </button>
                <button
                  onClick={() => {
                    this.props.themGioHang(phone);
                  }}
                  className="btn btn-outline-primary">
                  Thêm vào giỏ
                </button>
              </div>
            </div>
          </div>
        </div>
      );
    });
  };
  render() {
    console.log(this.props.data_phone);
    return <div className="row">{this.RenderPhonewithMap()}</div>;
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    themGioHang: (sanPham) => {
      const spGioHang = {
        maSP: sanPham.maSP,
        tenSP: sanPham.tenSP,
        giaBan: sanPham.giaBan,
        hinhAnh: sanPham.hinhAnh,
      };
      const action = {
        type: "THEM_GIO_HANG",
        spGioHang: spGioHang,
      };
      dispatch(action);
    },
    xemChiTiet: (sanPham) => {
      const action = {
        type: "XEM_CHI_TIET",
        spdetailed: sanPham,
      };
      dispatch(action);
    },
    thayDoiDienThoai: (maSP, index_img) => {
      const action = {
        type: "THAY_DOI_DIEN_THOAI",
        maSP,
        index_img,
      };
      dispatch(action);
    },
  };
};

const mapStateToProps = (state) => {
  return {
    data_phone: state.gioHangReducer.data_phone,
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(ItemPhone);
